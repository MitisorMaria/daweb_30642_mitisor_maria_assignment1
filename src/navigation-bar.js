import React from 'react'
import style from './commons/styles/style.css'
import logo from './commons/images/sigla.png';

import {
    Nav,
    Navbar,
    NavbarBrand,
    NavLink,
    UncontrolledDropdown
} from 'reactstrap';

const textStyle = {
    color: 'white',
    textDecoration: 'none'
};

const divStyle = {
    display: 'flex'
}

const NavigationBar = () => (
    <div>
        <Navbar color="dark" light expand="md">
            <NavbarBrand href="/">
                <img src={logo} width={"50"}
                     height={"35"} />
            </NavbarBrand>
            <Nav className="mr-auto" navbar>
                <ul className='nav navbar-nav navbar-inverse navbar-custom'>
                    <li><NavLink href="/acasa" style={textStyle}>Acasă</NavLink></li>
                    <li><NavLink href="/noutati" style={textStyle}>Noutăți</NavLink></li>
                    <li><NavLink href="/despre" style={textStyle}>Despre</NavLink></li>
                    <li><NavLink href="/profilStudent" style={textStyle}>Profil student</NavLink></li>
                    <li><NavLink href="/coordonator" style={textStyle}>Coordonator</NavLink></li>
                    <li> <NavLink href="/contact" style={textStyle}>Contact</NavLink></li>
                </ul>
            </Nav>
        </Navbar>
    </div>


);

export default NavigationBar
