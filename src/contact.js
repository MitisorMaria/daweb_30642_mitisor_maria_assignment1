import React from 'react';
import BackgroundImg from './commons/images/poza.jpg';
const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1000px",
    backgroundImage: `url(${BackgroundImg})`
};

const textStyle = {color: 'white', align: 'middle'};
class Contact extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
    }



    refresh(){
        this.forceUpdate()
    }

    render() {
        return (
            <div  style={backgroundStyle}>
                <h1 style={textStyle}>Contact</h1>
                <p style={textStyle}><b>Adresa: </b>str. George Barițiu 26-28, Cluj-Napoca</p>
                <p style={textStyle}><b>Nr. de telefon: </b>0767-356777</p>
                <p style={textStyle}><b>E-mail: </b>maria.mitisor@gmail.com</p>
                <footer>
                    <p className="copyright">© Maria Mitișor 2020</p>
                </footer>
            </div>
        );
    };

}

export default Contact;
