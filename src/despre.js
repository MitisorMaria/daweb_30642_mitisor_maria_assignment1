import React from 'react';
import BackgroundImg from './commons/images/poza.jpg';
const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1000px",
    backgroundImage: `url(${BackgroundImg})`
};

const textStyle = {color: 'white', align: 'middle'};

class Despre extends React.Component {

    constructor(props){
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.state = {
            collapseForm: true,
            loadPage: false,
            errorStatus: 0,
            error: null
        };
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
    }



    refresh(){
        this.forceUpdate()
    }

    render() {
        return (
            <div  style={backgroundStyle}>
                <h1 style={textStyle}>Despre lucrare</h1>
                <p  style={textStyle}> <b>Lucrarea consta in crearea unei aplicatii care sa poata identifica persoana care
                vorbeste intr-o inregistrare. Acest lucru va fi realizat cu ajutorul unei retele neuronale recurente.
                Inregistrarile din setul de date de antrenament, precum si cele din setul de test vor avea un singur vorbitor.
                    Tehnologia folosita este TensorFlow.</b></p>
                <p  style={textStyle}> <b>Exista mai multe tipuri de recunoastere de vorbitor, si anume:
                    <ul>
                        <li>
                            In functie de ce vorbitori avem: closed-set (luam in calcul doar cativa vorbitori pe care ii avem deja in baza de date)
                            sau open-set (daca dam peste o inregistrare a unui vorbitor necunoscut, il adaugam la restul vorbitorilor);
                        </li>
                        <li>
                            In functie de ceea ce se vorbeste: text-dependent sau text-independent;
                        </li>
                    </ul>

                    Aplicatia pe care o vom dezvolta este closed-set si text-dependent.
                </b> </p>

                <footer>
                    <p className="copyright">© Maria Mitișor 2020</p>
                </footer>
            </div>
        );
    };

}

export default Despre;
